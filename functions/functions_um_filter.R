# *******************************************************************************
# UltraMassExplorer (UME) by Tim Leefmann, Boris P. Koch, Stephan Frickenhaus
# Alfred Wegener Institute Helmholtz Centre for Polar and Marine Research
# Bremerhaven, Germany
# (c) AWI
# April, 2018
# 
# All rights reserved. This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see http://www.gnu.org/licenses/.
# *******************************************************************************

# =============================================================================
# This is a collection of functions used for formula subsetting (filter) 
# as a basis for the wrapper function __f_um_evaluation_pipe()
# =============================================================================

# =============================================================================
# Filter isotope numbers, element ratios, etc
# =============================================================================
f_filter <- function(df_orig,
                     file_id_min=1, file_id_max=9999999, 
                     file_id=NA, 
                     int13c_=0, int34s_=0,
                     ppm_=99,
                     dbe_max=999, 
                     mz_min=1, mz_max=9999,
                     n_min=0, n_max=999, s_min=0, s_max=999, p_min=0, p_max=999,
                     oc_min=0, oc_max=999, hc_min=0, hc_max=999, nc_max=99,
                     c13_max=999, s34_max=999, n15_max=999, 
                     cl35_max=999, cl37_max=999,
                     surfactant_=NA, 
                     blank_=NA, 
                     mf = "", known_mf = NA, known_mf_=NA,
                     n_occurrence_=1, 
                     msg=F
)
  
{
  
  file_id_regex<-paste(file_id,collapse="|")
  if(file_id_regex==""){file_id_regex<-NA}
  if(is.null(int13c_)){int13c_=0}
  if(is.null(int34s_)){int34s_=0}
  if(is.null(surfactant_)){surfactant_=999}
  if(known_mf_=="all")(show_only<-as.character(df_orig$mf))else(show_only<-as.character(known_mf[category %in% known_mf_]$mf))
  
  # apply filter:
    df <- df_orig[int13c>=int13c_
                & int34s>=int34s_
                & file_id %like% file_id_regex
                & !mf %in% as.character(known_mf[category %in% surfactant_]$mf)
                & surfactant<surfactant_
                & mf %in% show_only
                & !mf %in% as.character(df_orig[file_id %in% blank_]$mf)
                & abs(ppm) <= ppm_
                & dbe<=dbe_max
                & mz>=mz_min
                & mz<=mz_max
                & n>=n_min
                & n<=n_max
                & s>=s_min
                & s<=s_max
                & p>=p_min
                & p<=p_max
                & oc>=oc_min
                & oc<=oc_max
                & hc>=hc_min
                & hc<=hc_max
                & nc<=nc_max
                ,]

if(msg==T){ 
  print(paste("file_id_min", file_id_min, "file_id_max", file_id_max))
  print(paste("file_id", file_id))
  print(paste("int13c_", int13c_))
  print(paste("int34s_", int34s_))
  print(paste("ppm_", ppm_))
  print(paste("dbe_max", dbe_max))
  print(paste("mz_min", mz_min, "mz_max", mz_max))
  print(paste("n_min", n_min, "n_max", n_max))
  print(paste("s_min", s_min, "s_max", s_max))
  print(paste("p_min", p_min, "p_max", p_max))
  print(paste("oc_min", oc_min, "oc_max", oc_max))
  print(paste("hc_min", hc_min, "hc_max", hc_max))
  print(paste("nc_max", nc_max))
  print(paste("c13_max", c13_max))
  print(paste("s34_max", s34_max))
  print(paste("n15_max", n15_max))
  print(paste("surfactant_", surfactant_))
  }
  return(df)
}

# =============================================================================
# Filter intensities
# =============================================================================

f_filter_int <- function(df,
                         bp_min=0, bp_max=100,
                         sum_min=0, sum_max=100, 
                         sum_opt_min=0, sum_opt_max=9999999,
                         sum_rank_min=0, sum_rank_max=100, 
                         msg=F)
  
{

if(msg==T){
  print(paste("bp_min", bp_min, "bp_max", bp_max))
  print(paste("sum_min", sum_min, "sum_max", sum_max))
  print(paste("sum_opt_min", sum_opt_min, "sum_opt_max", sum_opt_max))
  print(paste("sum_rank_min", sum_rank_min, "sum_rank_max", sum_rank_max))
  }
  
if(df[,.N]>0){    
  # Select intensity threshold:
    df <- df[rel_int>=bp_min
             & rel_int<=bp_max
             & rel_sum_int>=sum_min
             & rel_sum_int<=sum_max
             & rel_sum_int_rank>=sum_rank_min
             & rel_sum_int_rank<=sum_rank_max
             ,]
    
    return(df)
  }
}

# =============================================================================
# Normalize dataset according to intensity-related parameters
# =============================================================================

f_normalize <- function(df, relint = "rel_int", n_rank=200)
  
{
  
  print("***************************************************")
  print(paste0("Normalizing dataset by (", relint, ")...")) 
  tic()
  
if(df[,.N]>0){
  # Calculate relative intensity: base peak magnitude
    df[, bp:=max(i_magnitude), by = .(file_id)]
    df[, rel_int:=i_magnitude*100/max(i_magnitude), by = .(file_id)]
  
  # Calculate relative intensity: sum of total intensity 
    df[, sum_int:=sum(i_magnitude), by = .(file_id)]
    df[, rel_sum_int:=i_magnitude*100/sum_int, by = .(file_id)]
  
  # Calculate relative intensity: sum of intensity for unique peaks
    df[, n_occurrence := .N, by = .(mf_iso)]
    # print(df[, .(max(n_occurrence),.N), by = .(file_id)])
  
  # Assign sum of magnitudes for ubiquitous peaks
    if ((length(df$sum_int_opt)>0)==T) {df$sum_int_opt<-NULL} # remove existing calculation
    df[n_occurrence==max(n_occurrence), sum_int_opt:=sum(i_magnitude), by=.(file_id)]
    df$sum_int_opt[is.na(df$sum_int_opt)] <- 0  # this is required to determine the maximum per file in the next step
  
  # Calculate rel. intensities for all peaks based on sum_int_opt
    df[, rel_sum_int_opt:=i_magnitude*100/max(sum_int_opt), by = .(file_id)]
  
  # Calculate relative intensity: sum of Top100 rank most intense peaks  
  # assign ranks based on i_magnitude
    df[, rank:=frank(-i_magnitude, ties.method = "min"), by=file_id]
    if ((length(df$sum_int_rank)>0)==T) {df$sum_int_rank<-NULL} # remove existing calculation

  # select TOPx magnitudes and calculate their sum of i_magnitude
    df[rank<=n_rank, sum_int_rank:=sum(i_magnitude), by = .(file_id)]
    df$sum_int_rank[is.na(df$sum_int_rank)] <- 0  # this is required to determine the maximum per file in the next step

  # Calculate rel. intensities for all peaks based on sum_int_rank
    df[, rel_sum_int_rank:=i_magnitude*100/max(sum_int_rank), by = .(file_id)]
  
  df[, norm_dat:=(i_magnitude-median(i_magnitude))/IQR(i_magnitude), by=.(file_id)]
  
  t_norm <<- df[, .(bp=round(max(bp)/1000000,0), sum_int=round(max(sum_int)/1000000,0), sum_int_opt=round(max(sum_int_opt)/1000000,0), sum_int_rank=round(max(sum_int_rank)/1000000000,0)), by=file_id]
  
  #renew assignments
    df[, n_assignments := .N, by = .(file_id, peak_id)]
  # df[, .(counts=.N), by=.(n_assignments)]
  
  # Select the normalization procedure from metadata  
    if (relint=="rel_int") {
      df[,ri:=rel_int]; ri_label <<- "'rel_int'"; ri_stats <<- "rel_int"  
    } else if (relint=="rel_sum_int") { 
      df[,ri:=rel_sum_int]; ri_label <<-  "'rel_sum_int'"; ri_stats <<- "rel_sum_int" 
    } else if (relint=="rel_sum_int_opt")  { 
      df[,ri:=rel_sum_int_opt]; ri_label <<- "'rel_sum_int_opt'"; ri_stats <<- "rel_sum_int_opt" 
    } else if (relint=="rel_sum_int_rank")  { 
      df[,ri:=rel_sum_int_rank]; ri_label <<- "'rel_sum_int_rank'"; ri_stats <<- "rel_sum_int_rank"
    } else if (relint=="i_magnitude")  { 
      df[,ri:=i_magnitude]; ri_label <<- "'i_magnitude'"; ri_stats <<- "i_magnitude" 
    } else 
      print("Entry for normalization procedure is missing!!! 
            (01_tab_sample_set; column: normalization)")
  
  # Renew column (ri) - basis for many plots and statistics    
    summary(df$ri); ri<<-df$ri
  
    toc()
    return(df)
  }
}

