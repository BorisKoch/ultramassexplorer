# *******************************************************************************
# UltraMassExplorer (UME) by Tim Leefmann, Boris P. Koch, Stephan Frickenhaus
# Alfred Wegener Institute Helmholtz Centre for Polar and Marine Research
# Bremerhaven, Germany
# (c) AWI
# April, 2018
# 
# All rights reserved. This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see http://www.gnu.org/licenses/.
# *******************************************************************************

# =============================================================================
# This is a collection of functions used for the required for data preparation 
# as a basis for data evaluation (called by function um_utilities.match())
# =============================================================================

# =============================================================================
# Add element/isotope columns that might be missing so far in df
# =============================================================================

f_um_calc_neutral_masses <- function(peaklist, pol, ppm_dev){
  
  print("***************************************************")
  print("Creating neutral mass list...")
  tic()
  
  if (pol=="neg")
  {
    peaklist[, m:=mz+1.0072763]
    peaklist[, m_min:=mz + 1.0072763-((mz + 1.0072763)*ppm_dev/1000000)]
    peaklist[, m_max := mz + 1.0072763 + ((mz + 1.0072763)*ppm_dev/1000000)]
  }
  
  if (pol=="pos") 
  {
    peaklist[, m:=mz-1.0072763]
    peaklist[, m_min:= mz-1.0072763-((mz-1.0072763)*ppm_dev/1000000)]
    peaklist[, m_max:=mz-1.0072763 + ((mz-1.0072763)*ppm_dev/1000000)]
  }
  
  if (pol=="neu") 
  {
    peaklist[, m:=mz]
    peaklist[, m_min:= mz-(mz*ppm_dev/1000000)]
    peaklist[, m_max:=mz+(mz*ppm_dev/1000000)]
  }
  return(peaklist)
  toc()
}

# =============================================================================
# Assign molecular formulas
# =============================================================================

f_um_assign_formulas <- function(peaklist, ultramassmf, UME, status.s_n, status.peak_id, status.file_id, lib_version){
  
  print("***************************************************")
  print("Assigning molecular formulas to masses...")
  tic()
  
  # make sure that peak magnitude is numeric
    peaklist$i_magnitude<-as.numeric(peaklist$i_magnitude)  
  
  # sort peaklist by mass
    setkey(peaklist,m)
  
  # assign peaklist masses
    s <- peaklist$m
    n <- length(s)
    dev <- (peaklist$m_max-peaklist$m_min)/2
  
  # assign library masses
    d <- ultramassmf$mass
  
  # ranges
    r.d <- range(d)
    r.s <- range(s)
  
  # check if sorted
    d.d <- diff(d)
    all(d.d >= 0)
    s.d <- diff(s)
    all(s.d >= 0)
  
  # insert s into d by index in d
  # check if range of samples (s) is within range of db (d)
    stopifnot (r.s[1] > r.d[1] && r.s[2] < r.d[2])
  
  # create vectors for indexing lower (pl) and upper limit (pt)
    pl = rep(NA,n)
    pt = rep(NA,n)
  
  # define start/end index
    #Kl = which.min(abs(d - dev[1]))
    Kl = which.min(abs(d-(s[1]-dev[1])))
    if (d[Kl]>s[1]-dev[1]){
      Kl=Kl-1
    }
    # Kt = which.min(abs(d + dev[1]))
    Kt = which.min(abs(d+(s[1]-dev[1])))
    if (d[Kt]>s[1] + dev[1]){
      Kt=Kt-1
    }
  
  # find elements in database whith lowest deviation
    if(UME==T) {setProgress(0.5, detail = "analyzing peaks")}
  
    for (I in 1:n){
      S=s[I]
      while(S - dev[I] > d[Kl]) {Kl = Kl + 1}
      while(S + dev[I] > d[Kt]) {Kt = Kt + 1}
      if (S + dev[I] > d[Kl] & S - dev[I] < d[Kt - 1]) {
        pl[I] = Kl;pt[I] = Kt - 1
      }
    }
  
    if(UME==T) {setProgress(0.75, detail = "list matches")}
  
  # create datatable of peaks and corresponding indexes
    if(status.s_n!="not available"){matches <- data.table(peaklist[,.(peak_id,file_id,i_magnitude,mz,s_n)], s, pl, pt)}
    if(status.s_n=="not available"){matches <- data.table(peaklist[,.(peak_id,file_id,i_magnitude,mz)], s, pl, pt)}
  
  # remove non matched peaks
    matches <- na.omit(matches)
  
  # create list for indeces
    L <- as.list(1:length(matches$s))
  
  # fill list with indeces of respective ranges
    P.range <- lapply(L,function(x) matches$pl[x]:matches$pt[x])
  
  # create list of matched keys of db
    P.Keys <- sapply(P.range,function(x) as.list(ultramassmf$vkey[x]));
  
    if(status.s_n!="not available"){
      # unlist matches into new datatable
        matchesunl <- data.table(peak_id = unlist(lapply(L, function (x) rep(matches$peak_id[x], sapply(P.Keys[x],length)))),
                             file_id = unlist(lapply(L, function (x) rep(matches$file_id[x], sapply(P.Keys[x],length)))),
                             i_magnitude = unlist(lapply(L, function (x) rep(matches$i_magnitude[x], sapply(P.Keys[x],length)))),
                             mz = unlist(lapply(L, function (x) rep(matches$mz[x], sapply(P.Keys[x],length)))),
                             s_n = unlist(lapply(L, function (x) rep(matches$s_n[x], sapply(P.Keys[x],length)))),
                             m = unlist(lapply(L, function (x) rep(matches$s[x], sapply(P.Keys[x],length))))
        )
    }
  
    if(status.s_n=="not available"){
    # unlist matches into new datatable
      matchesunl <- data.table(peak_id = unlist(lapply(L, function (x) rep(matches$peak_id[x], sapply(P.Keys[x],length)))),
                             file_id = unlist(lapply(L, function (x) rep(matches$file_id[x], sapply(P.Keys[x],length)))),
                             i_magnitude = unlist(lapply(L, function (x) rep(matches$i_magnitude[x], sapply(P.Keys[x],length)))),
                             mz = unlist(lapply(L, function (x) rep(matches$mz[x], sapply(P.Keys[x],length)))),
                             m = unlist(lapply(L, function (x) rep(matches$s[x], sapply(P.Keys[x],length))))
      )

  }
  
  # Bind matches with corresponding rows in ultramass indexed by vkey
    df <- data.table(matchesunl, ultramassmf[(unlist(P.Keys)-lib_version)])
    toc()
    return(df)
}

# =============================================================================
# Add element/isotope columns that might be missing so far in df
# =============================================================================

f_um_add_missing_element_columns <- function(df){
  # Define the required isotope columns:
  cols <- c("15n"
            #"na", 
            #"d", 
            #"35cl", "37cl", "79br", "81br",
            # "54fe", "56fe", "57fe", "59co", "63cu", "65cu", "232th"
  )
  
  # If isotope columns are not existing yet add column with value "0"
    for(j in cols) {if (j %in% colnames(df)==F) df[, (j):=0]}
    
  return(df)
}

# =============================================================================
# Build a molecular formula string according to the Hill system
# =============================================================================

f_um_create_mf_strings <- function(df){
  
  print("***************************************************")
  print("Building molecular formula strings...")
  tic()
  
# Build a molecular formula string according to the Hill system
  df$mf <- 
    paste0(
      ifelse(df[["c"]]+df[["13c"]]>0, paste0('C', ifelse(df[["c"]]+df[["13c"]]>1,df[["c"]]+df[["13c"]], '')), ''),
      ifelse(df[["h"]]>0, paste0('H', ifelse(df[["h"]]>1,df[["h"]], '')), ''),
      ifelse(df[["n"]]+df[["15n"]]>0, paste0('N', ifelse(df[["n"]]+df[["15n"]]>1,df[["n"]]+df[["15n"]], '')), ''),
      ifelse(df[["o"]]>0, paste0('O', ifelse(df[["o"]]>1,df[["o"]], '')), ''), 
      ifelse(df[["p"]]>0, paste0('P', ifelse(df[["p"]]>1,df[["p"]], '')), ''),
      ifelse(df[["s"]]+df[["34s"]]>0, paste0('S', ifelse(df[["s"]]+df[["34s"]]>1,df[["s"]]+df[["34s"]], '')), '')
    )

# Build a molecular formula string containing all isotopes  
  df$mf_iso <- 
    paste0(ifelse(df[["c"]]>0, paste0(' C', df[["c"]]), ''),
           ifelse(df[["13c"]]>0, paste0(' 13C', df[["13c"]]), ''), 
           ifelse(df[["h"]]>0, paste0(' H', df[["h"]]), ''),
           ifelse(df[["n"]]>0, paste0(' N', df[["n"]]), ''), 
           ifelse(df[["15n"]]>0, paste0(' 15N', df[["15n"]]), ''), 
           ifelse(df[["o"]]>0, paste0(' O', df[["o"]]), ''), 
           ifelse(df[["p"]]>0, paste0(' P', df[["p"]]), ''),                      
           ifelse(df[["s"]]>0, paste0(' S', df[["s"]]), ''),
           ifelse(df[["34s"]]>0, paste0(' 34S', df[["34s"]]), '')
    )
  toc()
  return(df)
}

# =============================================================================
# Calculate nominal mass
# =============================================================================

f_um_calculate_nm <- function(df){
  
print("***************************************************")
print("Calculating nominal mass...")
tic()
  
# Calculate the nominal mass    
  df[, nm:=c*12 + `13c`*13 + h + n*14 + o*16+ p*31 + s*32 + `34s`*34 + `15n`*15
  ]
  toc()
}

# =============================================================================
# Calculate other evaluation parameters such as dbe, element ratios etc
# =============================================================================

f_um_calculate_eval_params <- function(df){

print("***************************************************")
print("Calculating evaluation parameters...")  
tic()
  
# (Re-)calculate nominal mass
  f_um_calculate_nm(df)
  
# Calculate absolute and relative mass error  
  df[, del:=round(m - m_cal, 5)]
  df[, ppm:=round((m - m_cal) / (m_cal) * 1000000,4)]  
  
# Calculate Double Bond Equivalents (DBE) based on valences N=3, S=2, P=3
# If dbe column is not yet existing calculate dbe and add column 
# (trace elements not yet considered)
  if (!"dbe" %in% colnames(df)) {
    df[, dbe:= c + `13c` - (0.5 * (h
                                  #+d
                                  #+na
                                  #+`35cl`+`37cl`+`79br`+`81br`
    )) + (0.5 * (n + `15n` + p)) + 1]
  }
  
# Calculate element ratios
  df[(c+`13c`)>0, c("oc", "hc", "nc", "sc"):=
       .(round(o/(c+`13c`),3), round((h)/(c+`13c`),3), round((n+`15n`)/(c+`13c`),4),round((s+`34s`)/(c+`13c`),4))]
  
  df[,dbe_o:=dbe-o]
  
# Calculate aromaticity index  
  df[c+`13c`-o-s-`34s`-n-`15n`-p != 0, 
     ai:=round(1+c+`13c`-o-s-`34s`-(0.5*(h
                                         #+d+na
                                         +n+`15n`+p))/(c+`13c`-o-s-`34s`-n-`15n`-p),1)]
  df[c+`13c`-o-s-`34s`-n-`15n`-p == 0, ai:=-999]
  
  df[,wf:=dbe-o+(0.5*(nm%%14-14))]

# z* and Kendrick mass defect for CH2 and other units
  df[,z:=(nm%%14)-14]
  df[,kmd:=round(nm-(m*14/14.01565),4)]
  
# Nominal oxidation state of carbon and delG0ox (see LaRowe)  
  df[,nosc:=round(-1*((4*(c+`13c`)+h-(3*n+3*`15n`)-(2*o)+(5*p)-(2*s+2*`34s`))/(c+`13c`))+4,2)]
  df[,delg0_cox:=round(60.3-28.5*(-1*((4*(c+`13c`)+h-(3*n+3*`15n`)-(2*o)+(5*p)-(2*s+2*`34s`))/(c+`13c`))+4),2)]
  
# Calculate theoretical intensity of isotope signals based on moelcular formula  
  df[,relint13c_calc:=round(c*1.08,2)]
  df[,int13c_calc:=round(c*1.08*i_magnitude/100,0)]
  df[,relint32s_calc:=round(s*4.47,2)]
  df[,int32s_calc:=round(s*4.47*i_magnitude/100,0)]
  
# Assign different classes of heteroatom combinations  
  df[s+`34s`>0 & n+`15n`>0 & p>0,snp_check:='snp']
  df[s+`34s`>0 & n+`15n`>0 & p==0,snp_check:='sn']
  df[n+`15n`>0 & p>0 & s+`34s`==0,snp_check:='np']
  df[s+`34s`>0 & p>0 & n+`15n`==0,snp_check:='sp']
  df[s+`34s`==0 & p==0 & n+`15n`==0,snp_check:='']
  
# Assign all combinations heteroatoms    
  df[,nsp_type:=paste0("N", df$n, " S", df$s, " P", df$p)]
  
# Calculate total number of atoms
  df[,c_tot:=c+`13c`]
  df[,s_tot:=s+`34s`]
  df[,n_tot:=n+`15n`]
  
# Create column "n_occurrence": in how many samples of the dataset does a formula occur?
  df <- df[, n_occurrence_orig := .N, by = .(mf_iso_o = mf_iso)]
  
# Create column number of assignments per peak (based on raw data)
  df <- df[, n_assignments_orig := length(peak_id), by = .(a = file_id, b = peak_id)]  
  
  toc()
  return(df)
  
}

# =============================================================================
# Evaluate isotope information
# =============================================================================

f_um_eval_isotopes <- function(df){
  
  ### add isotope information and remove isotopoloques
  
  print("***************************************************")
  print("Evaluate isotope information...")
  tic()
  
  if (max(df$`13c` > 0)){
    tab_check_13c <-  dcast(df, file_id + mf ~ `13c`, value.var = "i_magnitude", fun=mean, fill=0)
    setnames(tab_check_13c, c("0", "1"), c("int12c", "int13c"))
    df <- tab_check_13c[df, on = c("mf", "file_id")]
    df[int13c>0,dev_n_c:=round((((int13c*100/int12c)/1.08) - c),2)]
    df[int13c==0,dev_n_c:=999] # Assign standard value for parent ions without daughter
    df <- df[!(int12c == 0 & int13c > 0),] # remove daughter without parent
    rm(tab_check_13c)
  }else{df[,c("int12c", "int13c"):=list(rep(NA, dim(df)[1]), rep(0, dim(df)[1]))]}
  
  if(max(df$`34s` > 0)){
    tab_check_34s <-  dcast(df[s+`34s`>0], file_id + mf ~ `34s`, value.var = "i_magnitude", fun = mean, fill = 0)
    setnames(tab_check_34s, c("0", "1"), c("int32s", "int34s"))
    df <- tab_check_34s[df, on = c("mf", "file_id")]
    df[is.na(int32s),int32s:=0]
    df[is.na(int34s),int34s:=0]
    df <- df[!(int32s == 0 & int34s > 0),] # remove daughter without parent
    df[, int34s_val:=1]
    df[int32s>0 & int34s==0, int34s_val:=0] # Lable for S-formulas which are not verified by S34
    rm(tab_check_34s)
  }else{df[,c("int32s", "int34s"):=list(rep(NA, dim(df)[1]), rep(0, dim(df)[1]))]}
  
  # Format columns containing the isotope information  
    df[, c("int32s", "int12c", "m", "m_cal"):=.(round(int32s,0), round(int12c,0), m, m_cal)]
  
  # Remove all isotope daughters (still contained in the "intxy"-columns)
    df<-df[`13c`==0,]
    df<-df[`34s`==0,]
    df<-df[`15n`==0,]
    #df<-df[`37cl`==0,]
    #df<-df[`81br`==0,]
  
  toc()
  return(df)
    
}

# =============================================================================
# Add metainformation derived from known_mf
# =============================================================================

f_um_add_known_mf <- function(df, known_mf){
  
  print("***************************************************")
  print("Adding information about known molecular formulas...") 
  tic()
  
# Select columns from database table containing known molecular formulas  
  known_mf <- known_mf[category %in% c("surfactant", "ideg_neg", "ideg_pos", "iterr2_neg", "iterr2_pos", "IOS")] 
  
# Restructure table containing known molecular formulas  
  known_mf <- dcast(known_mf, mf~category, fun.aggregate = length, fill=0, value.var = "mf_id")
  
# Add information of known molecular formulas to formula table
  df <- known_mf[, .(mf, surfactant, ideg_neg, ideg_pos, iterr2_neg, iterr2_pos, IOS)][df, on="mf"]
  df[is.na(surfactant), surfactant:=0]
  df[is.na(IOS), IOS:=0]
  
  toc()
  return(df)
}

# =============================================================================
# Order columns
# =============================================================================

f_um_order_columns <- function(df){

  setcolorder(df,
              c("file_id", "mz", "i_magnitude", "m", "m_cal", "ppm", "nm", "mf", "dbe", 
                "c", "h", "n", "o", "p", "s", "hc", "oc", "nc", "sc", "ai", "z", "kmd",  
                colnames(df)[!(colnames(df) %in% 
                                 c("file_id", "mz", "i_magnitude", "m", "m_cal", "ppm", "nm", "mf", "dbe", 
                                   "c", "h", "n", "o", "p", "s", "hc", "oc", "nc", "sc", "ai", "z", "kmd"
                                 ))]))

  return(df)
}
