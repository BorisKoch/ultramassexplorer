$(document).ready(function() {
  // Change input field
  $('#ppm_dev')[0].type = 'text';
  // Replace "," by "."
  $('#ppm_dev').keyup(function(event) {
    $(this).val($(this).val().replace(/,/g, '.'));
  });
});