# *******************************************************************************
# UltraMassExplorer (UME) by Tim Leefmann, Boris P. Koch, Stephan Frickenhaus
# Alfred Wegener Institute Helmholtz Centre for Polar and Marine Research
# Bremerhaven, Germany
# (c) AWI
# April, 2018
# 
# All rights reserved. This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see http://www.gnu.org/licenses/.
# *******************************************************************************

body <- dashboardBody(style="display: block; overflow: auto;",
  #============================================================================
  # rawdata view layout
  #============================================================================
  hidden(div(id="rawdata_box", box(
    width = 12,
    div(style = 'overflow-x: scroll',
        DT::dataTableOutput('rawdata_table'))
  ))),
  
  #============================================================================
  # selected data view layout
  #============================================================================
  hidden(div(id = "selected_data_box", box(
    width = 12,
    div(style = 'overflow-x: scroll', DT::dataTableOutput('selected_data_table'))
  ))), 
  
  #============================================================================
  # results view layout
  #============================================================================
  hidden(div(
    id = "results_box",
    tabsetPanel(id="results_tabset",
                tabPanel('Tables',
                  tabsetPanel(
                    selected = "Report preview",
                    # report preview layout
                    tabPanel(
                      "Report preview",
                      verbatimTextOutput("create_report")
                    ),
                    tabPanel("Input data",
                             downloadButton("download_input_data", label="csv", class="downl_butt"),
                             div(style = 'overflow-x: scroll', DT::dataTableOutput("input_data_table"))
                    ),
                    # peaklist statistics view layout
                    tabPanel(
                      "Peaklist statistics",
                      downloadButton("download_peaklist_statistics_data", label="csv", class="downl_butt"),
                      div(style = 'overflow-x: scroll', DT::dataTableOutput("peaklist_statistics"))
                    ),
                    # table unfiltered data view layout
                    tabPanel(
                      "Unfiltered data",
                      downloadButton("download_unfiltered_data", label="csv", class="downl_butt"),
                      div(style = 'overflow-x: scroll', DT::dataTableOutput('unfiltered_data_table'))
                    ),
                    # table normalized & filtered data view layout
                    tabPanel(
                      "Filtered data",
                      downloadButton("download_normalized_filtered_data", label="csv", class="downl_butt"),
                      div(style = 'overflow-x: scroll', DT::dataTableOutput('normalized_filtered_data_table'))
                    ),
                    # cluster view layout
                    tabPanel(
                      "Pivot",
                      downloadButton("download_pivot_table", label="csv", class="downl_butt"),
                      div(style = 'overflow-x: scroll', DT::dataTableOutput("pivot_table"))
                    ),
                    # aggregated data view layout
                    tabPanel(
                      "Aggregated data",
                      downloadButton("download_data_summary", label="csv", class="downl_butt"),
                      div(style = 'overflow-x: scroll', DT::dataTableOutput("data_summary"))
                    )
                  )
                ),
                tabPanel('Plots',
                  tabsetPanel(
                        tabPanel(
                          "Reconstructed spectra",
                          box(
                            dropdownButton(
                              selectInput("colpal_f_plotFT_rel_int_by_norm", "Color scale", UMEcolorscales, selected = "awi"),
                              colourInput("col_f_plotFT_rel_int_by_norm", "Color", value = "black"),
                              circle = F, status = "danger", size = "xs", icon = icon("gear")
                            ),
                            withSpinner(plotOutput("f_plotFT_rel_int_by_norm", height = 600)), 
                            width=12
                          )
                        ),
                        
                        # quality view layout
                        tabPanel(
                          "Quality",
                          box(dropdownButton(selectInput("colpal_nomf_vs_sample", "Color scale", UMEcolorscales, selected = "awi"),
                                             colourInput("col_nomf_vs_sample", "Color"),
                                             circle = F, status = "danger", size = "xs", icon = icon("gear")
                          ),
                          plotOutput("um_plot.nomf_vs_sample")),
                          box(dropdownButton(selectInput("colpal_ri_vs_sample", "Color scale", UMEcolorscales, selected = "awi"),
                                             colourInput("col_ri_vs_sample", "Color"),
                                             circle = F, status = "danger", size = "xs", icon = icon("gear")
                          ),
                          plotOutput("um_plot.ri_vs_sample")),
                          box(dropdownButton(tags$a(href="http://doi.org/10.1021/ac061949s", "cf. Koch et al. 2007", icon("info-sign", lib="glyphicon"),  target="_blank"),
                                             selectInput("colpal_isotope_precision", "Color scale", UMEcolorscales, selected = "awi"),
                                             selectInput("z_var_isotope_precision", "Color variable", c("")),
                                             checkboxInput("log_isotope_precision", "LOG", F),
                                             checkboxInput("col_bar_isotope_precision", "Colorbar", value=T),
                                             sliderInput("point_size_isotope_precision",
                                                         label="Point size",
                                                         min=0.5,
                                                         max=1.5,
                                                         value=1,
                                                         step=0.1),
                                             circle = F, status = "danger", size = "xs", icon = icon("gear")
                          ),
                          plotOutput("um_plot.isotope_precision")
                          ),
                          box(dropdownButton(selectInput("colpal_f_plotFT_NSP", "Color scale", UMEcolorscales, selected = "awi"),
                                             colourInput("col_f_plotFT_NSP", "Color"),
                                             circle = F, status = "danger", size = "xs", icon = icon("gear")
                          ),
                          plotOutput("f_plotFT_NSP"))
                        ),
                        
                        # frequency view layout
                        tabPanel(
                          "Frequency",
                          box(
                            dropdownButton(
                              selectInput("colpal_dbe_frequency", "Color scale", UMEcolorscales, selected = "awi"),
                              colourInput("col_dbe_frequency", "Color"),
                              circle = F, status = "danger", size = "xs", icon = icon("gear")
                            ),
                            plotOutput("um_plot.dbe_frequency")
                          ),
                          box(
                            dropdownButton(
                              selectInput("colpal_freq_vs_ppm", "Color scale", UMEcolorscales, selected = "awi"),
                              colourInput("col_freq_vs_ppm", "Color"),
                              circle = F, status = "danger", size = "xs", icon = icon("gear")
                            ),
                            plotOutput("um_plot.freq_vs_ppm")),
                          box(
                            dropdownButton(
                              selectInput("colpal_s_n_frequency", "Color scale", UMEcolorscales, selected = "awi"),
                              colourInput("col_s_n_frequency", "Color"),
                              circle = F, status = "danger", size = "xs", icon = icon("gear")
                            ),
                            plotOutput("um_plot.s_n_frequency")),
                          box(
                            dropdownButton(
                              selectInput("colpal_rel_int_frequency", "Color scale", UMEcolorscales, selected = "awi"),
                              colourInput("col_rel_int_frequency", "Color"),
                              circle = F, status = "danger", size = "xs", icon = icon("gear")
                            ),
                            plotOutput("um_plot.rel_int_frequency"))
                        ),
                        
                        # element frequency view layout
                        tabPanel(
                          "Element frequency",
                          box(
                            dropdownButton(
                              selectInput("colpal_n_atom_frequency", "Color scale", UMEcolorscales, selected = "awi"),
                              colourInput("col_n_atom_frequency", "Color"),
                              circle = F, status = "danger", size = "xs", icon = icon("gear")
                            ),
                            plotOutput("um_plot.n_atom_frequency")),
                          box(
                            dropdownButton(
                              selectInput("colpal_s_atom_frequency", "Color scale", UMEcolorscales, selected = "awi"),
                              colourInput("col_s_atom_frequency", "Color"),
                              circle = F, status = "danger", size = "xs", icon = icon("gear")
                            ),
                            plotOutput("um_plot.s_atom_frequency")),
                          box(
                            dropdownButton(
                              selectInput("colpal_p_atom_frequency", "Color scale", UMEcolorscales, selected = "awi"),
                              colourInput("col_p_atom_frequency", "Color"),
                              circle = F, status = "danger", size = "xs", icon = icon("gear")
                            ),
                            plotOutput("um_plot.p_atom_frequency")),
                          box(
                            dropdownButton(
                              selectInput("colpal_o_atom_frequency", "Color scale", UMEcolorscales, selected = "awi"),
                              colourInput("col_o_atom_frequency", "Color"),
                              circle = F, status = "danger", size = "xs", icon = icon("gear")
                            ),
                            plotOutput("um_plot.o_atom_frequency"))
                        ),
                        
                        # van Krevelen plots
                        tabPanel(
                          "Van Krevelen",
                          box(dropdownButton(tags$a(href="http://doi.org/10.1021/ac034415p", "cf. Kim et al. 2003", icon("info-sign", lib="glyphicon"), target="_blank"),
                                             selectInput("col_vk_1",label = "Color scale", choices = UMEcolorscales),
                                             selectInput("z_var_vk_1", "Color variable", c("")),
                                             checkboxInput("log_vk_1", "LOG", F),
                                             checkboxInput("median_vk_1", "MEDIAN", F),
                                             checkboxInput("ai_vk_1", "AI", F),
                                             checkboxInput("col_bar_vk_1", "Colorbar", value=T),
                                             checkboxInput("projection_vk_1", "Projection", value=T),
                                             circle = F, status = "danger", size = "xs", icon = icon("gear")),
                              plotOutput("um_plot.vk_1", click="plot_click.vk_1", brush=brushOpts("plot_brush.vk_1", resetOnNew = T))),
                          box(dropdownButton(tags$a(href="http://doi.org/10.1021/ac034415p", "cf. Kim et al. 2003", icon("info-sign", lib="glyphicon"), target="_blank"),
                                             selectInput("col_vk_2",label = "Color scale", choices = UMEcolorscales),
                                             selectInput("z_var_vk_2", "Color variable", c("")),
                                             checkboxInput("log_vk_2", "LOG", F),
                                             checkboxInput("median_vk_2", "MEDIAN", F),
                                             checkboxInput("ai_vk_2", "AI", F),
                                             checkboxInput("col_bar_vk_2", "Colorbar", value=T),
                                             checkboxInput("projection_vk_2", "Projection", value=T),
                                             circle = F, status = "danger", size = "xs", icon = icon("gear")),
                              plotOutput("um_plot.vk_2", click="plot_click.vk_2", brush=brushOpts("plot_brush.vk_2", resetOnNew = T))),
                          box(hidden(div(id = "click_selected.vk_1", 
                                         fluidRow(column(8,
                                                         div(style = 'overflow-y: scroll', verbatimTextOutput("click.vk_1"))),
                                                  column(4,
                                                    div(style="margin-bottom:1em", selectInput("select_click.vk_1", label = NULL, choices =c(""))),
                                                    div(style="margin-bottom:1em", uiOutput("pubchem_beta_link.vk_1"))
                                                  )
                                         )
                          )
                          ),
                          hidden(div(id = "brush_selected.vk_1",
                                     fluidRow(
                                       column(8,
                                              verbatimTextOutput("brush.vk_1")),
                                       column(4,
                                              downloadButton("download_brush.vk_1"))
                                     )))
                          ),
                          box(hidden(div(id = "click_selected.vk_2", 
                                         fluidRow(column(8,
                                                         verbatimTextOutput("click.vk_2")),
                                                  column(4,
                                                         div(style="margin-bottom:1em", selectInput("select_click.vk_2", label = NULL, choices =c(""))),
                                                         div(style="margin-bottom:1em", uiOutput("pubchem_beta_link.vk_2"))
                                                  )
                                         )
                          )
                          ),
                          hidden(div(id = "brush_selected.vk_2",
                                     fluidRow(
                                       column(8,
                                              verbatimTextOutput("brush.vk_2")),
                                       column(4,
                                              downloadButton("download_brush.vk_2"))
                                     )))
                          )
                        ), 
                        
                        # van krevelen 3D view layout
                        tabPanel(
                          "Van Krevelen 3D",
                          dropdownButton(selectInput("col_vk3d",label = "Color scale", choices = UMEcolorscales),
                                         selectInput("vk3d_z", "Z axis variable", c("")),
                                         selectInput("vk3d_z_col", "Color variable", c("")),
                                         checkboxInput("log_vk3d", "LOG", value=F),
                                         #checkboxInput("projection_vk3d", "Projection", value=T),
                                         sliderInput("point_size_vk3d",
                                                     label="Point size",
                                                     min=0.1,
                                                     max=1,
                                                     value=0.2,
                                                     step=0.1),
                                         circle = F, status = "danger", size = "xs", icon = icon("gear")
                          ),
                          withSpinner(scatterplotThreeOutput("vk3d", height="80vh")) 
                        ),
                        # KMD layout
                        tabPanel(
                          "Kendrick",
                          box(dropdownButton(tags$a(href="http://doi.org/10.1021/ac026106p", "cf. Stenson et al. 2003", icon("info-sign", lib="glyphicon"), target="_blank"),
                                             selectInput("col_kmd_vs_nm",label = "Color scale", choices = UMEcolorscales),
                                             selectInput("z_var_kmd_vs_nm", "Color variable", c("")),
                                             checkboxInput("log_kmd_vs_nm", "LOG", F),
                                             checkboxInput("col_bar_kmd_vs_nm", "Colorbar", value=T),
                                             circle = F, status = "danger", size = "xs", icon = icon("gear")
                              ),
                              plotOutput("um_plot.kmd_vs_nm", click="plot_click.kmd_vs_nm", brush=brushOpts("plot_brush.kmd_vs_nm", resetOnNew = T))),
                          box(dropdownButton(tags$a(href="http://doi.org/10.1021/ac026106p", "cf. Stenson et al. 2003", icon("info-sign", lib="glyphicon"), target="_blank"),
                                             selectInput("z_var_kmd_vs_nm_zoom", "Color variable", c("")),
                                             checkboxInput("log_kmd_vs_nm_zoom", "LOG", F),
                                             checkboxInput("col_bar_kmd_vs_nm_zoom", "Colorbar", value=T),
                                             circle = F, status = "danger", size = "xs", icon = icon("gear")
                              ),
                              plotOutput("um_plot.kmd_vs_nm_zoom", click="plot_click.kmd_vs_nm_zoom", brush=brushOpts("plot_brush.kmd_vs_nm_zoom", resetOnNew = T))),
                          box(hidden(div(id = "click_selected.kmd_vs_nm", 
                                         fluidRow(column(8,
                                                         verbatimTextOutput("click.kmd_vs_nm")
                                         ),
                                         column(
                                           4,
                                           div(style="margin-bottom:1em", selectInput("select_click.kmd_vs_nm", label = NULL, choices =c(""))),
                                           div(style="margin-bottom:1em", uiOutput("pubchem_beta_link.kmd_vs_nm"))
                                         )
                                         )
                              )
                              ),
                              hidden(div(id = "brush_selected.kmd_vs_nm",
                                         fluidRow(
                                           column(8,
                                                  verbatimTextOutput("brush.kmd_vs_nm")),
                                           column(4,
                                                  downloadButton("download_brush.kmd_vs_nm"))
                                         )))
                          ),
                          box(hidden(div(id = "click_selected.kmd_vs_nm_zoom", 
                                         fluidRow(column(8,
                                                         verbatimTextOutput("click.kmd_vs_nm_zoom")
                                         ),
                                         column(
                                           4,
                                           div(style="margin-bottom:1em", selectInput("select_click.kmd_vs_nm_zoom", label = NULL, choices =c(""))),
                                           div(style="margin-bottom:1em", uiOutput("pubchem_beta_link.kmd_vs_nm_zoom"))
                                         )
                                         )
                          )
                          ),
                          hidden(div(id = "brush_selected.kmd_vs_nm_zoom",
                                     fluidRow(
                                       column(8,
                                              verbatimTextOutput("brush.kmd_vs_nm_zoom")),
                                       column(4,
                                              downloadButton("download_brush.kmd_vs_nm_zoom"))
                                     )))
                          )
                        ),
                        # mass plots view layout
                        tabPanel(
                          "Mass",
                          box(dropdownButton(tags$a(href="http://doi.org/10.1021/ac101444r", "cf. Schmitt-Kopplin et al., 2010,", icon("info-sign", lib="glyphicon"), target="_blank"),
                                             selectInput("col_hcvM",label = "Color scale", choices = UMEcolorscales),
                                             selectInput("z_var_hcvM", "Color variable", c("")),
                                             checkboxInput("log_hcvM", "LOG", F),
                                             checkboxInput("col_bar_hcvM", "Colorbar", value=T),
                                             circle = F, status = "danger", size = "xs", icon = icon("gear")
                              ),
                              plotOutput("um_plot.hcvM", click="plot_click.hcvM", brush=brushOpts("plot_brush.hcvM", resetOnNew = T))),
                          box(dropdownButton(tags$a(href="http://doi.org/10.1016/j.chroma.2009.02.033", "cf. Reemtsma 2009", icon("info-sign", lib="glyphicon"), target="_blank"),
                                             selectInput("col_CvM",label = "Color scale", choices = UMEcolorscales),
                                             selectInput("z_var_CvM", "Color variable", c("")),
                                             checkboxInput("log_CvM", "LOG", F),
                                             checkboxInput("col_bar_CvM", "Colorbar", value=T),
                                             circle = F, status = "danger", size = "xs", icon = icon("gear")
                              ),
                              plotOutput("um_plot.CvM", click="plot_click.CvM", brush=brushOpts("plot_brush.CvM", resetOnNew = T))),
                          box(hidden(div(id = "click_selected.hcvM", 
                                         fluidRow(column(8,
                                                         verbatimTextOutput("click.hcvM")
                                         ),
                                         column(
                                           4,
                                           div(style="margin-bottom:1em", selectInput("select_click.hcvM", label = NULL, choices =c(""))),
                                           div(style="margin-bottom:1em", uiOutput("pubchem_beta_link.hcvM"))
                                         )
                                         )
                          )
                          ),
                          hidden(div(id = "brush_selected.hcvM",
                                     fluidRow(
                                       column(8,
                                              verbatimTextOutput("brush.hcvM")),
                                       column(4,
                                              downloadButton("download_brush.hcvM"))
                                     )))
                          ),
                          box(hidden(div(id = "click_selected.CvM",
                                         fluidRow(
                                           column(8,
                                                  verbatimTextOutput("click.CvM")),
                                           column(
                                             4,
                                             selectInput(
                                               "select_click.CvM",
                                               label = NULL,
                                               choices = c("")
                                             ),
                                             div(style="margin-bottom:1em", uiOutput("pubchem_beta_link.CvM"))
                                           )
                                         ))),
                              hidden(div(id = "brush_selected.CvM",
                                         fluidRow(
                                           column(8,
                                                  verbatimTextOutput("brush.CvM")),
                                           column(4,
                                                  downloadButton("download_brush.CvM"))
                                         )))
                          )
                        ),
                        # DBE plots view layout
                        tabPanel(
                          "DBE",
                          box(dropdownButton(tags$a(href="http://doi.org/10.1007/s00216-014-8249-y", "cf. Herzsprung et al. 2014", icon("info-sign", lib="glyphicon"), target="_blank"),
                                             selectInput("col_dbe_vs_o",label = "Color scale", choices = UMEcolorscales),
                                             selectInput("z_var_dbe_vs_o", "Color variable", c("")),
                                             checkboxInput("log_dbe_vs_o", "LOG", F),
                                             checkboxInput("col_bar_dbe_vs_o", "Colorbar", value=T),
                                             circle = F, status = "danger", size = "xs", icon = icon("gear")
                          ),
                          plotOutput("um_plot.dbe_vs_o", click="plot_click.dbe_vs_o", brush=brushOpts("plot_brush.dbe_vs_o", resetOnNew = T))),
                          box(dropdownButton(tags$a(href="http://doi.org/10.1007/s00216-014-8249-y", "cf. ????Proteomics", icon("info-sign", lib="glyphicon"), target="_blank"),
                                             selectInput("col_dbe_vs_c",label = "Color scale", choices = UMEcolorscales),
                                             selectInput("z_var_dbe_vs_c", "Color variable", c("")),
                                             checkboxInput("log_dbe_vs_c", "LOG", F),
                                             checkboxInput("col_bar_dbe_vs_c", "Colorbar", value=T),
                                             circle = F, status = "danger", size = "xs", icon = icon("gear")
                          ),
                          plotOutput("um_plot.dbe_vs_c", click="plot_click.dbe_vs_c", brush=brushOpts("plot_brush.dbe_vs_c", resetOnNew = T))),
                          box(hidden(div(id = "click_selected.dbe_vs_o", 
                                         fluidRow(column(8,
                                                         verbatimTextOutput("click.dbe_vs_o")
                                         ),
                                         column(
                                           4,
                                           div(style="margin-bottom:1em", selectInput("select_click.dbe_vs_o", label = NULL, choices =c(""))),
                                           div(style="margin-bottom:1em", uiOutput("pubchem_beta_link.dbe_vs_o"))
                                         )
                                         )
                          )
                          ),
                          hidden(div(id = "brush_selected.dbe_vs_o",
                                     fluidRow(
                                       column(8,
                                              verbatimTextOutput("brush.dbe_vs_o")),
                                       column(4,
                                              downloadButton("download_brush.dbe_vs_o"))
                                     )))
                          ),
                          box(hidden(div(id = "click_selected.dbe_vs_c", 
                                         fluidRow(column(8,
                                                         verbatimTextOutput("click.dbe_vs_c")
                                         ),
                                         column(
                                           4,
                                           div(style="margin-bottom:1em", selectInput("select_click.dbe_vs_c", label = NULL, choices =c(""))),
                                           div(style="margin-bottom:1em", uiOutput("pubchem_beta_link.dbe_vs_c"))
                                         )
                                         )
                          )
                          ),
                          hidden(div(id = "brush_selected.dbe_vs_c",
                                     fluidRow(
                                       column(8,
                                              verbatimTextOutput("brush.dbe_vs_c")),
                                       column(4,
                                              downloadButton("download_brush.dbe_vs_c"))
                                     )))
                          )
                        ),
                        
                        # cluster view layout
                        tabPanel(
                          "Statistics",
                          withSpinner(plotOutput("cluster", height="600px"))
                        )
                    )
                  )
  )
  )
  )
)
