# *******************************************************************************
# UltraMassExplorer (UME) by Tim Leefmann, Boris P. Koch, Stephan Frickenhaus
# Alfred Wegener Institute Helmholtz Centre for Polar and Marine Research
# Bremerhaven, Germany
# (c) AWI
# April, 2018
# 
# All rights reserved. This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see http://www.gnu.org/licenses/.
# *******************************************************************************

f_um_eval_pipe <- function(df_orig, file_id_min=1, file_id_max=9999999, file_id=NA, 
                           int13c_=0, int34s_=0, 
                           ppm_=99,
                           dbe_max=999, 
                           mz_min=1, mz_max=9999, 
                           n_min=0, n_max=999, s_min=0, s_max=999, p_min=0, p_max=999, 
                           oc_min=0, oc_max =99, hc_min=0, hc_max=99, nc_max=99,
                           c13_max=999, s34_max=999, n15_max=999,
                           cl35_max=999, cl37_max=999,
                           surfactant_=NA, 
                           blank_=NA,
                           relint ="rel_int", n_rank=200, ubiquitous = F,
                           bp_min=0, bp_max=9999999, sum_min=0, sum_max=9999999, 
                           sum_opt_min=0, sum_opt_max=9999999, sum_rank_min=0, sum_rank_max=9999999,
                           known_mf = NA, known_mf_=NA,
                           mf = "",
                           n_occurrence_=1,
                           online = F, save = F)
  
{
  
  print(paste0(df_orig[,.N], " formulas before filter (df_orig; step 1)"))
  
  df <- f_filter(df_orig=df_orig, file_id_min=file_id_min, file_id_max=file_id_max, file_id=file_id,
                 int13c_=int13c_, int34s_=int34s_,
                 ppm_=ppm_,
                 dbe_max=dbe_max,
                 mz_min=mz_min, mz_max=mz_max, 
                 n_min=n_min, n_max=n_max, s_min=s_min, s_max=s_max, p_min=p_min, p_max=p_max,
                 oc_min=oc_min, oc_max=oc_max, hc_min=hc_min, hc_max=hc_max, nc_max=nc_max,
                 c13_max=c13_max, s34_max=s34_max, n15_max=n15_max, 
                 cl35_max=cl35_max, cl37_max=cl37_max, 
                 blank_ = blank_,
                 mf=mf, known_mf = known_mf, known_mf_=known_mf_,
                 surfactant_ = surfactant_
                 )
  
  print(paste0(df[,.N], " formulas after filter and before normalization (df, step 2)"))
  
  df <- f_normalize(df, relint, n_rank = n_rank)
  
  print(paste0(df[,.N], " formulas after first normalization (step 3)"))
  validate(need(!is.null(df), "No data to display. Adjust filter settings!"))
  df <- f_filter_int(df,
                     bp_min=bp_min, bp_max=bp_max,
                     sum_min=sum_min, sum_max=sum_max,
                     sum_opt_min=sum_opt_min, sum_opt_max=sum_opt_max,
                     sum_rank_min=sum_rank_min, sum_rank_max=sum_rank_max)
  
  print(paste0(df[,.N], " formulas after intensity filter (step 4)"))
  
# Calculate the number of occurrences of each MF in the dataset
# and filter according to "n_occurrence_" 
  
  if (df[,.N]>1 & n_occurrence_>1) {
    
# print(paste0("No. of occurrences of a formula within dataset set to:   ", n_occurrence_))
    df[, n_occurrence := .N, by = .(mf_iso)]
    df <- df[n_occurrence>=n_occurrence_,]
  }
  
  print(paste0(df[,.N], " formulas after occurrence update (step 5)"))
  
# Normalize data (each filter process requires a subsequent normalization)  
  df <- f_normalize(df, relint, n_rank = n_rank)
  print(paste0(df[,.N], " formulas after second normalization (step 6)"))
  
  #save(df, file = "df_ume.RData")
  return(df)
  
}
