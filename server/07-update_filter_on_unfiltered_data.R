# *******************************************************************************
# UltraMassExplorer (UME) by Tim Leefmann, Boris P. Koch, Stephan Frickenhaus
# Alfred Wegener Institute Helmholtz Centre for Polar and Marine Research
# Bremerhaven, Germany
# (c) AWI
# April, 2018
# 
# All rights reserved. This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see http://www.gnu.org/licenses/.
# *******************************************************************************

#============================================================================
# update filter settings on input
#============================================================================  

reac <- reactiveValues(redraw = FALSE)

observeEvent(unfiltered_data(),{
  updateSliderInput(session,
                    inputId = "n_filter",
                    value = c(0, max(as.integer(unfiltered_data()$n))),
                    max = max(as.integer(unfiltered_data()$n)),
                    step = 1)
  updateSliderInput(session,
                    inputId = "s_filter",
                    value = c(0, max(as.integer(unfiltered_data()$s))),
                    max = max(as.integer(unfiltered_data()$s)),
                    step = 1)
  updateSliderInput(session,
                    inputId = "p_filter",
                    value = c(0, max(as.integer(unfiltered_data()$p))),
                    max = max(as.integer(unfiltered_data()$p)),
                    step = 1)
  updateSliderInput(session,
                    inputId = "oc_filter",
                    value = c(0, (round(max(unfiltered_data()$`oc`), 1)+0.1)),
                    max = round(max(unfiltered_data()$`oc`), 1)+0.1,
                    step = 0.1)
  updateSliderInput(session,
                    inputId = "hc_filter",
                    value = c(0,(round(max(unfiltered_data()$`hc`), 1)+0.1)),
                    max = round(max(unfiltered_data()$`hc`), 1)+0.1,
                    step = 0.1)
  updateSliderInput(session,
                    inputId = "n_occurrence_filter",
                    value = min(as.integer(normalized_filtered_data()$`n_occurrence_orig`)),
                    #max = max(as.integer(normalized_filtered_data()$`n_occurrence_orig`)),
                    max = length(unique(normalized_filtered_data()$`file_id`)),
                    step = 1)
  updateSliderInput(session,
                    inputId = "dbe_max_filter",
                    value = max(as.integer(unfiltered_data()$`dbe`))+1,
                    max = max(as.integer(unfiltered_data()$`dbe`))+1,
                    step = 1)
  updateNumericInput(session,
                     inputId = "mz_filter_low",
                     value = min(as.integer(unfiltered_data()$mz))-1,
                     min = min(as.integer(unfiltered_data()$mz))-1,
                     max = max(as.integer(unfiltered_data()$mz))+1
  )
  updateNumericInput(session,
                     inputId = "mz_filter_top",
                     value = max(as.integer(unfiltered_data()$mz))+1,
                     min = min(as.integer(unfiltered_data()$mz))-1,
                     max = max(as.integer(unfiltered_data()$mz))+1
  )
  updateTextInput(session,
                  inputId = 'ppm__filter',
                  value=paste0(input$ppm_dev)
  )
  delay(4000, reac$redraw <- TRUE)
})

observeEvent(input$normalization,{
  updateTextInput(session, inputId = "bp_min", value="0")
  updateTextInput(session, inputId = "bp_max", value="100")
  updateTextInput(session, inputId = "sum_min", value="0")
  updateTextInput(session, inputId = "sum_max", value="100")
  updateTextInput(session, inputId = "sum_opt_min", value="0")
  updateTextInput(session, inputId = "sum_opt_max", value="9999999")
  updateTextInput(session, inputId = "sum_rank_min", value="0")
  updateTextInput(session, inputId = "sum_rank_max", value="100")
}
)

observeEvent(input$filter_profiles,{
  reac$redraw <- FALSE
  if(input$filter_profiles=="none"){
    updateSliderInput(session,
                      inputId = "n_filter",                         
                      value = c(min(as.integer(unfiltered_data()$n)), max(as.integer(unfiltered_data()$n)))
                      )
    updateSliderInput(session,
                      inputId = "s_filter",                         
                      value = c(min(as.integer(unfiltered_data()$s)), max(as.integer(unfiltered_data()$s)))
                      )
    updateSliderInput(session,
                      inputId = "p_filter",                         
                      value = c(min(as.integer(unfiltered_data()$p)), max(as.integer(unfiltered_data()$p)))
                      )
    updateSliderInput(session,
                      inputId = "oc_filter",                         
                      value = c(0, (max(round(unfiltered_data()$`oc`, 1))+0.1))
                      )
    updateSliderInput(session,
                      inputId = "hc_filter",                         
                      value = c(0,(max(round(unfiltered_data()$`hc`, 1))+0.1))
                      )
  
    updateSliderInput(session,
                      inputId = "n_occurrence_filter",                         
                      value = min(as.integer(normalized_filtered_data()$`n_occurrence_orig`)),
                      max = length(unique(normalized_filtered_data()$`file_id`)), 
                      step = 1)
    updateSliderInput(session,
                      inputId = "dbe_max_filter",                         
                      value = max(as.integer(unfiltered_data()$`dbe`))+1,
                      step = 1)
    updateNumericInput(session,
                       inputId = "mz_filter_low",
                       value = min(as.integer(unfiltered_data()$mz))-1
                       )
    updateNumericInput(session,
                       inputId = "mz_filter_top",
                       )
    updateCheckboxGroupInput(session,
                             inputId = "surfactant__filter",
                             choices = c("Remove"="surfactant"))
    updatePickerInput(session,
                      inputId = 'known_mf_filter',
                      selected = "")
    updateCheckboxGroupInput(session,
                             inputId = "int13c_filter",
                             choices = c("Remove"=1))
    updateTextInput(session,
                    inputId = 'bp_min',
                    value="0")
    updateTextInput(session,
                    inputId = 'bp_max',
                    value="9999999")
  }

  if(input$filter_profiles=="standard_dom"){
    updateSliderInput(session,
                      inputId = "n_filter",                         
                      value = c(0,2),
                      step = 1)
    updateSliderInput(session,
                      inputId = "s_filter",                         
                      value = c(0,1),
                      step = 1)
    updateSliderInput(session,
                      inputId = "p_filter",                         
                      value = c(0,0),
                      step = 1)
    updateSliderInput(session,
                      inputId = "oc_filter",                         
                      value = c(0,(max(round(unfiltered_data()$`oc`, 1))+0.1)),
                      # max = max(round(unfiltered_data()$`oc`, 1))+0.1, 
                      step = 0.1)
    updateSliderInput(session,
                      inputId = "hc_filter",                         
                      value = c(0,(max(round(unfiltered_data()$`hc`, 1))+0.1)),
                      # max = max(round(unfiltered_data()$`hc`, 1))+0.1, 
                      step = 0.1)
    updateSliderInput(session,
                      inputId = "n_occurrence_filter",                         
                      value = min(as.integer(normalized_filtered_data()$`n_occurrence_orig`)),
                      max = length(unique(normalized_filtered_data()$`file_id`)), 
                      step = 1)
    updateSliderInput(session,
                      inputId = "dbe_max_filter",                         
                      value = 20,
                      step = 1)
    updateNumericInput(session,
                       inputId = "mz_filter_low",
                       value = 200
                       )
    updateNumericInput(session,
                       inputId = "mz_filter_top",
                       value = 700
                       )
    updateCheckboxGroupInput(session,
                             inputId = "surfactant__filter",
                             selected = "surfactant")
    updatePickerInput(session,
                      inputId = 'known_mf_filter',
                      selected = '')
    updateCheckboxGroupInput(session,
                             inputId = "int13c_filter",
                             selected = 1)
   updateTextInput(session,
                    inputId = 'bp_min',
                    value="0")
    updateTextInput(session,
                    inputId = 'bp_max',
                    value="100")
  }
  delay(4000, reac$redraw <- TRUE)
}
)