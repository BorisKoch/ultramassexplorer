# *******************************************************************************
# UltraMassExplorer (UME) by Tim Leefmann, Boris P. Koch, Stephan Frickenhaus
# Alfred Wegener Institute Helmholtz Centre for Polar and Marine Research
# Bremerhaven, Germany
# (c) AWI
# April, 2018
# 
# All rights reserved. This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see http://www.gnu.org/licenses/.
# *******************************************************************************


#============================================================================
# app control structure
#============================================================================
observe({
  if (is.null(input$file1)) {
    shinyjs::hide("next_1")
    shinyjs::hide("file_input_options")
  }else{
    if(dim(rawdata())[1]>500000){
      shinyjs::hide("demomode")
      shinyjs::hide("next_1")
      shinyjs::hide("file_input_options")
      shinyjs::hide("rawdata_box")
      showModal(modalDialog(
        title = "Reduce size of input data set!",
        "Input is limited to a maximum number of 500,000 mass peaks.",
        easyClose = TRUE,
        size="m"
      ))
    }else{
      shinyjs::hide("demomode")
      shinyjs::show("file_input_options")
      shinyjs::show("rawdata_box")
      enable("mz_col")
      enable("i_magnitude_col")
      enable("file_id_col")
      enable("peak_id_col")
      enable("s_n_col")
      enable("pol")
      enable("ppm_dev")
      if (ncol(rawdata())<2) {
        shinyjs::hide("next_1")
      } else {
        shinyjs::show("next_1")  
      }
    }
  }
})

observeEvent(input$next_1, {
  hide("next_1") 
  hide("file_input_options")
  hide("data")
  show("next_2") 
  show("back_2")
  show("columns")}
)

observeEvent(input$demomode, {
  hide("demomode") 
  hide("file_input_options")
  hide("data")
  show("next_2")
  show("rawdata_box")
  show("columns")
  disable("mz_col")
  disable("i_magnitude_col")
  disable("file_id_col")
  disable("peak_id_col")
  disable("s_n_col")
  disable("pol")
  disable("ppm_dev")
  }
)

observeEvent(input$next_2, {
  hide("next_2") 
  hide("back_2")
  hide("columns")
  hide("rawdata_box")
  show("next_3")
  show("back_3")
  show("parameters")
  show("selected_data_box")}
)
observeEvent(input$back_2, {
  hide("next_2")
  hide("back_2")
  hide("columns")
  show("next_1") 
  show("data")
}
)

observeEvent(input$next_3, {
  validate(need(ppm_dev()>0, "Maximum mass error must be >0 ppm and <3 ppm."),
           need(ppm_dev()<3, "Maximum mass error must be >0 ppm and <3 ppm."))
  showModal(modalDialog(
      title = "Calculate molecular formulas?",
      "Press OK to calculate molecular formulas. Depending on the size of the input data set, this may take a few minutes.",
      easyClose = TRUE,
      size="m",
      footer =  tagList(
        modalButton("CANCEL"),
        actionButton("ok", "OK")
      )
    )
  )
})

observeEvent(input$ok, {
  hide("next_3") 
  hide("back_3")
  hide("parameters")
  hide("selected_data_box")
  hide("data")
  hide("empty_lines")
  show("close")
  show("back_4")
  show("filter_norm")
  show("results_box")
  removeModal()
})

observeEvent(input$back_3, {
  hide("next_3")
  hide("back_3")
  hide("parameters")
  hide("selected_data_box")
  show("next_2") 
  show("back_2")
  show("columns")
  show("rawdata_box")
}
)

observeEvent(input$back_4, {
  hide("next_4") 
  hide("back_4")
  hide("filter_norm")
  hide("results_box")
  show("next_3")
  show("back_3")
  show("parameters")
  show("selected_data_box")}
)

observeEvent(input$close, {
  showModal(modalDialog(
    title = "Restart UltraMassExplorer?",
    "Press STOP APP to stop the app. All calculated results will be deleted. 
    Press CANCEL to continue with the current analysis.",
    easyClose = TRUE,
    size="m",
    footer =  tagList(
      modalButton("CANCEL"),
      actionButton("stopapp", "STOP APP")
    )
  ))
}
)

ultramassmf <- reactive({
  if(input$libmf=="lib_01"){load("lib/lib_01.RData")}
  if(input$libmf=="lib_02"){load("lib/lib_02.RData")}
  if(input$libmf=="lib_03"){load("lib/lib_03.RData")}
  if(input$libmf=="lib_04"){load("lib/lib_04.RData")}
  if(input$libmf=="lib_05"){load("lib/lib_05.RData")}
  if(input$libmf=="standard"){load("lib_standard.RData")}
  if(input$libmf=="standard_extended"){load("lib_standard_extended.RData")}
  if(input$libmf=="full_mode"){load("lib_full_mode.RData")}
  if(input$libmf=="known_compounds"){load("lib_known_compounds.RData")}
  return(ultramassmf)
})

lib_version<-reactive({ultramassmf()$vkey[1]-1})



observeEvent(input$stopapp, {js$closeApp()
                             stopApp()
                             })

observeEvent(input$timoutstopjs,{
  js$closeApp()
  stopApp()})