# UltraMassExplorer ([UME][])

*Interactive evaluation of high‐resolution mass spectrometric data*

- [How to cite](README.md#how-to-cite)
- [Local install](README.md#local-install)
- [Quick start video](README.md#quick-start-video)
- [Web application](README.md#web-application)
- [Publication](README.md#publication)
- [Researchgate project](README.md#researchgate-project)
- [R packages](README.md#r-packages)

---
### How to cite

Leefmann T, Frickenhaus S, Koch BP. UltraMassExplorer ‐ a browser‐based 
application for the evaluation of high‐resolution mass spectrometric data. 
_Rapid Commun Mass Spectrom._ 2019;33:193‐202. https://doi.org/10.1002/rcm.8315


---
### Local install

1. Download and install [R].

2. Download and install 
[RStudio](https://www.rstudio.com/products/rstudio/download/).

3. **Win users only:** download and install [RTools][].

   Activate the tick for _Edit System Path_ during installation ([details][]).

4. Use Git to clone UltraMassExplorer repository 
`git clone git@gitlab.com:BorisKoch/ultramassexplorer.git` **or** download the UME source 
code as [zip](https://gitlab.com/BorisKoch/ultramassexplorer/-/archive/master/ultramassexplorer.zip)
and extract to local hard drive. If you downloaded UME as zip, you have to
download all files from the *lib* directory separateley as these are tracked
using [lfs](https://git-lfs.github.com/).

5. Start [RStudio][], navigate to the directory where you cloned UME 
or extracted the zip archive, and set folder as working directory by 
running `setwd("path-to-folder")`. If you used the zip download, use the 
separately downloaded files from the *lib* directory to replace dummie files 
in the extracted *lib* folder. If you used `git clone` to clone the repository 
and want to use the code version that was published with the RCM manuscript
use `git checkout tags/1.0.0 -b rcm` to create a branch called *rcm* comprising the respective 
code version.

6. In [RStudio][], navigate to the *settings* directory and run 
*UME_install_script.R* **or** download 
[UME_install_script.R](settings/UME_install_script.R) and run in [RStudio][].

7. After all packages have been installed, open _ui.R_ from the the directory
where you stored the UltraMassExplorer files and press _Run App>Run External_ 
in the source tab to start UME in your web browser.


---
### Quick start video
https://www.awi.de/fileadmin/user_upload/AWI/Forschung/Biowissenschaft/Oekologische_Chemie/UME/UME_01_quickstart.mp4


---
### Web application
[![](www/ume_link.png)](https://ume.awi.de)


---
### Publication
[<img src="https://wol-prod-cdn.literatumonline.com/pb-assets/journal-banners/10970231-1501384659217.jpg" height="75">](https://onlinelibrary.wiley.com/doi/abs/10.1002/rcm.8315)


---
### R packages

Currently the following versions of R packages are installed in the docker image running the UME web application.

| Package | version |
| -------- | -------- |
| assertthat   | 0.2.0   |
| backports |	01.01.02 | 
| base64enc |	0.1-3 |
| BH | 1.66.0-1 |
| calibrate | 01.07.02 |
| cli |	1.0.0 |
| colorspace | 1.3-2 |
| colourpicker | 1.0 |
| crayon | 01.03.04 |
| crosstalk |	1.0.0 |
| curl |3.2 |
| data.table | 01.11.04 |
| DBI |	1.0.0
| devtools | 1.13.6 | 
| dichromat | 2.0-0 | 
| digest | 0.6.15 | 
| docopt | 0.4.5 | 
| dotCall64 | 0.9-5.2 | 
| DT | 0.4 | 
| ellipse |	0.4.1 | 
| evaluate | 0.10.1 | 
| fansi | 0.2.3 | 
| fields | 9.6 | 
| ggplot2 | 3.0.0 | 
| ggthemes | 4.0.0 | 
| git2r | 0.23.0 | 
| gridExtra | 2.3 | 
| gtable | 0.2.0 | 
| highr | 0.7 | 
| htmltools | 0.3.6 | 
| htmlwidgets |	1.2 | 
| httpuv |	1.4.4.2 | 
| httr | 01.03.01 | 
| igraph | 01.01.00 | 
| irlba | 02.03.02 | 
| jsonlite | 1.5 | 
| knitr | 1.20 | 
| labeling | 0.3 | 
| later | 0.7.3 | 
| lazyeval | 0.2.1 | 
| maps | 03.03.00 | 
| markdown | 0.8 | 
| memoise | 01.01.00 | 
| mime | 0.5 | 
| miniUI | 0.1.1.1 | 
| munsell | 0.5.0 | 
| openssl | 1.0.1 | 
| permute | 0.9-4 | 
| pillar | 01.03.00 | 
| pkgconfig | 2.0.1 | 
| plyr | 01.08.04 | 
| png | 0.1-7 | 
| promises | 1.0.1 | 
| purrr | 0.2.5 | 
| R6 | 02.02.02 | 
| RColorBrewer | 1.1-2 | 
| Rcpp | 0.12.17 | 
| reshape2 | 01.04.03 | 
| RJSONIO | 1.3-0 | 
| rlang | 0.2.1 | 
| rmarkdown | 1.10 | 
| RMySQL | 0.10.15 | 
| rprojroot | 1.3-2 | 
| rstudioapi | 0.7 | 
| scales | 0.5.0 | 
| scatterD3 | 0.8.2 | 
| shiny | 01.01.00 | 
| shinyBS | 0.61 | 
| shinycssloaders | 0.2.0 | 
| shinyCustom | 0.1.0 | 
| shinydashboard | 0.7.0 | 
| shinyjs | 1.0 | 
| shinysky | 0.1.2 | 
| shinythemes | 01.01.01 | 
| shinyWidgets | 0.4.3 | 
| sourcetools | 0.1.7 | 
| spam | 2.2-0 | 
| stringr | 01.03.01 | 
| threejs | 0.3.2 | 
| tibble | 01.04.02 | 
| tictoc | 1.0 | 
| tinytex | 0.6 | 
| utf8 | 01.01.04 | 
| V8 | 1.5 | 
| vegan | 2.5-2 | 
| viridis | 0.5.1 | 
| viridisLite | 0.3.0 | 
| whisker | 0.3-2 | 
| withr | 02.01.02 | 
| xfun | 0.3 | 
| xtable | 1.8-2 | 
| yaml | 02.01.19 | 
| glue | 01.02.00 | 
| littler | 0.3.3 | 
| magrittr | 1.5 | 
| stringi | 01.02.03 | 
| stringr | 01.03.01 | 
| base | 03.05.01 | 
| boot | 1.3-20 | 
| class | 7.3-14 | 
| cluster | 2.0.7-1 | 
| codetools | 0.2-15 | 
| compiler | 03.05.01 | 
| datasets | 03.05.01 | 
| foreign | 0.8-70 | 
| graphics | 03.05.01 | 
| grDevices | 03.05.01 | 
| grid | 03.05.01 | 
| KernSmooth | 2.23-15 | 
| lattice | 0.20-35 | 
| MASS | 7.3-50 | 
| Matrix | 1.2-14 | 
| methods | 03.05.01 | 
| mgcv | 1.8-24 | 
| nlme | 3.1-137 | 
| nnet | 7.3-12 | 
| parallel | 03.05.01 | 
| rpart | 4.1-13 | 
| spatial | 7.3-11 | 
| splines | 03.05.01 | 
| stats | 03.05.01 | 
| stats4 | 03.05.01 | 
| survival |2.42-4 | 
| tcltk | 03.05.01 | 
| tools | 03.05.01 | 
| utils | 03.05.01 | 


[details]:https://github.com/stan-dev/rstan/wiki/Install-Rtools-for-Windows
[R]:https://cloud.r-project.org/
[RStudio]:https://www.rstudio.com
[RTools]:https://cran.r-project.org/bin/windows/Rtools/
[UME]:https://www.awi.de/en/ume
