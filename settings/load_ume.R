# *******************************************************************************
# UltraMassExplorer (UME) by Tim Leefmann, Boris P. Koch, Stephan Frickenhaus
# Alfred Wegener Institute Helmholtz Centre for Polar and Marine Research
# Bremerhaven, Germany
# (c) AWI
# April, 2018
# 
# All rights reserved. This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see http://www.gnu.org/licenses/.
# *******************************************************************************

library(shiny)
library(data.table)
library(shinythemes)
library(ggplot2)
library(ggthemes)
library(threejs)
library(shinydashboard)
library(shinyjs)
library(colourpicker)
library(shinyBS)
library(shinycssloaders)
library(viridis)
library(httr)
library(DT)
library(vegan)
library(shinyCustom)
library(shinyWidgets)
library(scatterD3)
library(htmlwidgets)
library(fields)
library(V8)
library(stringr)
library(gridExtra) # for logo text
library(scales)
library(tictoc)

source("_f_um_docker_ports.R")

source("functions/functions_um_utilities.R")
source("functions/functions_um_filter.R")
source("functions/functions_um_plot.R")

source("_f_um_utilities.R")
source("___f_um_evaluation_pipe.R")

source("_f_um_download.R")
source("_f_create_report.R")
source("_f_update_indeces.R")
source("_f_um_create_data_summary.R")
source("js/java_script_code.R")

load("lib/known_mf.RData")
load("settings/tab_ume_labels.RData")
tab_ume_labels[, category:=label]
tab_ume_labels[, category_lable:=nice_label]
known_mf <- tab_ume_labels[known_mf, on = "category"]

known_mf_named_vector<-unique(known_mf[use_in_ume>0,category])
names(known_mf_named_vector)<-unique(known_mf[use_in_ume>0,nice_label])

load("settings/export_list_orig.RData")
export_list_orig<-export_list_orig[file_names!="ratios"]

load("settings/nice_labels.RData", .GlobalEnv)
UMEcolorscales<-c("awi","bk","gray","inferno","rainbow","ratios","terrain.colors","viridis")
