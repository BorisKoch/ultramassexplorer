# *******************************************************************************
# UltraMassExplorer (UME) by Tim Leefmann, Boris P. Koch, Stephan Frickenhaus
# Alfred Wegener Institute Helmholtz Centre for Polar and Marine Research
# Bremerhaven, Germany
# (c) AWI
# April, 2018
# 
# All rights reserved. This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License, version 3,
# as published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see http://www.gnu.org/licenses/.
# *******************************************************************************

# This script installs all R libraries required for UltraMassExplorer
# Notice: License

# Definition of packages
  required_packages <- c(
    # basic stuff
      "devtools",
    # for data pipeline
      "data.table","calibrate","plyr","tictoc", 
    # for figures and stats
      "vegan","fields","maps","png","httr","rgl","ggplot2","ggthemes","V8",
    # for figure settings
      "gridExtra","scales",
    # for shiny
      "shiny", "shinythemes", "shinydashboard","shinyWidgets","shinycssloaders",
      "shinyjs", "colourpicker","shinyBS","htmlwidgets","htmltools",
    # for render tables and figures
      "DT", "scatterD3",
    "viridis"
  )     

# Install and load packages that are not installed yet 
  for (i in required_packages){
    if(!is.element(i, .packages(all.available = TRUE))) {
      install.packages(i)
    }
    library(i,character.only = TRUE)
  }

# Specific package calls from other locations
  if (!require("igraph")){
    devtools::install_github("igraph/rigraph")
    library(igraph)}
  
  if (!require("threejs")){
    devtools::install_github("bwlewis/rthreejs")
    library(threejs)}
  
  if (!require("shinyCustom")){
    devtools::install_github("homerhanumat/shinyCustom")
    library(shinyCustom)}
  
  
# When using windows, the export function requires Rtools. Further information:  
  #=============================================================================
  # Install RTools (https://cran.r-project.org/bin/windows/Rtools/)
  # Make sure to activate the tick for "Edit System Path"
  # More details for windows installation: 
  # https://github.com/stan-dev/rstan/wiki/Install-Rtools-for-Windows
  